Shop4@docker
==================================

# How to run

You have two options to run the environment, depending mainly on your host OS. Essentially, you can either run the containers on bare metal, or through a virtualised environment.
 
## Linux

If you run Linux, you have both choices available to you. Running directly has certain advantages, not least of which the fact there's essentially zero overhead and full access to your system's muscle.

The advantage of running through a virtualised environment is mainly having your whole environment neatly packed into a single set of files for the virtual machine.

The choice is up to you. If you'd rather run the containers directly:

  * Ensure you have the latest `docker engine` installed. Your distribution's package might be a little old, if you encounter problems, do upgrade. See [https://docs.docker.com/engine/installation/](https://docs.docker.com/engine/installation/)
  * Ensure you have the latest `docker-compose` installed. See [docs.docker.com/compose/install](https://docs.docker.com/compose/install/)
  

On Ubuntu you might want to manage docker as a non-root user. For this, run `sudo usermod -aG docker $USER` and log out. Otherwise you might get permission errors when running docker commands.
You can test this via `docker run hello-world`.
  
Once you're done, simply `cd` to the `phpdocker` folder, then run `docker-compose up -d`. This will initialise and start all the containers, then leave them running in the background.

## Other OS

MacOS and Windows have no native support for docker containers. Get [Vagrant](https://www.vagrantup.com/) and [VirtualBox](https://www.virtualbox.org/) and install both of them.

Simply `cd` to the phpdocker folder within your clone of this repository and execute `vagrant up`. This will fire up [boot2docker](http://boot2docker.io/), then initialise the containers within. On OSX you can `vagrant ssh` to act on the containers from then on.
On windows you will need something like putty and follow the instructions the command `vagrant ssh` will give you.

As an alternative you can use [Docker Toolbox](https://www.docker.com/products/docker-toolbox) which will install and configure Virtualbox for you.
Note that the default IP address will probably be **192.168.99.100** then.

## Possible errors when using Vagrant

If you get an error message like `Client credential too weak` execute `vagrant reload`.
If vagrant halts at `Mounting NFS shared folders` execute `vagrant reload`.
If `docker ps -a` shows `Exited (255)` for the php container, retry via `docker-compose up -d`.

## After installation/composing

Open up [192.168.33.124:8080](http://192.168.33.124:8080/) or [localhost:8080](http://localhost:8080/) in your browser, depending on your configuration.
You will probably see a "Please wait" message. Wait a couple of minutes until all demo data is generated and reload the page.
You can login to the shop backend with the credentials shop4-dev/shop4-dev.
The shop's file system is exposed via the `public` folder. You can add plugins, modify files etc. and all the changes will be immediately visible within the shop.
  
## Accessing the database
  
You can use the phpMyAdmin container for executing custom SQL scripts by opening [192.168.33.124:8081](http://192.168.33.124:8081/) or [localhost:8081](http://localhost:8081/) in your Browser.
The user name and password both are "shop4-dev".

## General notes

The shop is configured for dev mode. This means it's not going to be very fast.
If you want to speed things up, disable SMARTY_FORCE_COMPILE and activate the object cache. But the whole config is by no means intended to be production ready.

## Adjusting demo data

Before calling `docker-compose up` you can set the environment variables CC, AC and HC to adjust the amount of demo data to be generated.

Examples: 
`export AC=5` to generate a total of 5 articles
`export MC=9` to generate a total of 9 manufacturers
`export CC=20` to generate a total of 20 categories

## Configuring the shop

You can use the caching methods memcached and redis. Set the redis host to `shop4-dev-redis` and the memcache(d) host to `shop4-dev-memcached`. After saving these options, both methods should become available.

## Services exposed outside your environment

You can access your application via **`localhost`**, if you're running the containers directly, or through **`192.168.33.124`** when run on a vm. nginx responds to any hostname, in case you want to add your own hostname on your `/etc/hosts`.

Service|Hostname|Address outside containers|Address outside VM (Vagrant)|Address outside VM (Docker Toolbox)
------|---------|---------|-----------|-----------
Webserver|shop4-dev-webserver|[localhost:8080](http://localhost:8080)|[192.168.33.124](http://192.168.33.124)|[192.168.99.100:8080](http://192.168.99.100:8080)
Webserver (SSL)|shop4-dev-webserver|[localhost:4443](https://localhost:4443)|[192.168.33.124](https://192.168.33.124)|[192.168.99.100:4443](https://192.168.99.100:4443)
phpMyAdmin|shop4-dev-phpmyadmin|[localhost:8081](http://localhost:8081)|[192.168.33.124:8081](http://192.168.33.124:8081)|[192.168.99.100:8081](http://192.168.99.100:8081)

**Note:** The SSL certificate is self-signed so you will have to ignore your browser's security warnings or import it into your own certification storage ([Tutorial for Ubuntu](https://leehblue.com/add-self-signed-ssl-google-chrome-ubuntu-16-04/)).

## Known issues

Microsoft Edge will probable not be able to show the shop when using Docker Toolbox.
A work-around can be found at [Common Issues and Fixes](https://github.com/docker/kitematic/wiki/Common-Issues-and-Fixes#windows-10).

## Hosts within your environment

You'll need to configure your application to use any services you enabled:

Service|Hostname|Port number
------|---------|-----------
php-fpm|shop4-dev-php-fpm|9000
MySQL|shop4-dev-mysql|3306 (default)
Memcached|shop4-dev-memcached|11211 (default)
Redis|shop4-dev-redis|6379 (default)

# Docker compose cheatsheet

**Note 1:** You need to cd first to where your docker-compose.yml file lives

**Note 2:** If you're using Vagrant, you'll need to ssh into it first

  * Start containers in the background: `docker-compose up -d`
  * Start containers on the foreground: `docker-compose up`. You will see a stream of logs for every container running.
  * Stop containers: `docker-compose stop`
  * Kill containers: `docker-compose kill`
  * View container logs: `docker-compose logs`
  * Execute command inside of container: `docker exec -it shop4-dev-php-fpm COMMAND` where `COMMAND` is whatever you want to run. For instance, `/bin/bash` to open a console prompt.
  * View nginx logs: `docker exec -i shop4-dev-webserver tail -f /var/log/nginx/access.log -f /var/log/nginx/error.log`
  * Rebuild `docker-compose build --force-rm --no-cache`
  * Clean up all the mess: `docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q) && docker volume rm $(docker volume ls -qf dangling=true)` 
