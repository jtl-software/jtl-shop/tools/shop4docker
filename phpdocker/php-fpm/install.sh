#!/bin/bash

configFile="/var/www/shop4-dev/public/includes/config.JTL-Shop.ini.php"
if [ -e "$configFile" ]
then
  echo "$configFile already exists - skipping installation."
else
  echo "$configFile does not exist - installing shop with $CATCOUNT categories, $MANUFCOUNT manufacturers and $ARTICLECOUNT articles..."
  echo "<h2>Please wait...</h2><br>Installing shop and generating $CATCOUNT categories, $MANUFCOUNT manufacturers and $ARTICLECOUNT articles" > /var/www/shop4-dev/public/index.htm
  sleep 20
  echo "Executing shopcli..."
  /usr/bin/shopcli shop:install --target-overwrite --install-demo-data \
      --admin-user="shop4-dev" --admin-password="shop4-dev" \
      --sync-user="shop4-dev" --sync-password="shop4-dev" \
      --database-host=shop4-dev-mysql --database-name=shop4-dev --database-user=shop4-dev --database-password=shop4-dev \
      --target-owner="www-data:www-data" --target-dir=/var/www/shop4-dev/public/ --enable-dev-mode >> /var/www/shop4-dev/public/index.htm
  echo "Done."
  echo "Installing demo data..."
  echo "OK.<br>Installing demo data... " >> /var/www/shop4-dev/public/index.htm
  /usr/bin/shopcli shop:install:sample-data \
      --article-count=$ARTICLECOUNT --category-count=$CATCOUNT --manufacturer-count=$MANUFCOUNT \
      --target-dir=/var/www/shop4-dev/public/ --target-overwrite >> /var/www/shop4-dev/public/index.htm
  echo "Done."
  echo "Cloning PluginBootstrapper plugin..."
  echo "OK.<br>Cloning Plugin JTL PluginBootstrapper... " >> /var/www/shop4-dev/public/index.htm
  rm -rf /var/www/shop4-dev/public/includes/plugins/jtl_plugin_bootstrapper
  rm -rf /var/www/shop4-dev/public/xhprof*
  git clone https://gitlab.jtl-software.de/jtlshop/JTLpluginBootstrapper.git /var/www/shop4-dev/public/includes/plugins/jtl_plugin_bootstrapper
  chown -R www-data:www-data /var/www/shop4-dev/public/includes/plugins/jtl_plugin_bootstrapper
  echo "Done."
  echo "Cloning xhprof directories..."
  echo "OK.<br>Getting xhprof... " >> /var/www/shop4-dev/public/index.htm
  wget -q https://gitlab.jtl-software.de/f.moche/xhprof/repository/archive.tar.gz?ref=master -O /tmp/xhprof.tar.gz
  tar -xvvzf /tmp/xhprof.tar.gz -C /var/www/shop4-dev/public/ --strip-components=1
  chown -R www-data:www-data /var/www/shop4-dev/public/xhprof*
  echo "Done."
  echo "Getting demo shop logo..."
  wget -q https://demo.jtl-shop.de/bilder/intern/shoplogo/shoplogo.png -O /var/www/shop4-dev/public/bilder/intern/shoplogo/transparent.png
  echo "Done."
  echo "OK.<br>Cleaning up... " >> /var/www/shop4-dev/public/index.htm
  rm -f /var/www/shop4-dev/public/index.htm
  echo "Installation finished."
fi
