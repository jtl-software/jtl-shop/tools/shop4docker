upstream _php {
    server shop4-dev-php-fpm:9000;
}

server {
    listen 80 default;
    listen *:443 ssl;

    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';
    ssl_prefer_server_ciphers on;
    ssl_session_cache shared:SSL:10m;

    ssl_certificate /etc/nginx/localhost.pem;
    ssl_certificate_key /etc/nginx/localhost.key;

    client_max_body_size 72M;

    sendfile off;

    access_log /var/log/nginx/shop4-dev.access.log;

    root /var/www/shop4-dev/public;

    index index.htm index.php;

    location ~ /\. {
        deny all;
        access_log off;
        log_not_found off;
    }

    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location ~* \.(eot|ttf|woff|css|less)$ {
        expires max;
        add_header Access-Control-Allow-Origin *;
        add_header Pragma public;
        access_log off;
        log_not_found off;
    }

    location @img_proxy {
        rewrite ^(.*)$ /index.php;
    }

    location ~ \.(gif|jpg|jpeg|png)$ {
        root /var/www/shop4-dev/public;
        try_files $uri @img_proxy;
        expires max;
        add_header Pragma public;
        access_log off;
        log_not_found off;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
        expires max;
        add_header Pragma public;
        access_log off;
        log_not_found off;
    }

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    location ~ \.php$ {
        try_files /89c20a338bb23891f9c7f9fb1f4d7a0b.htm @php;
    }

    location @php {
        fastcgi_pass _php;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PHP_VALUE "error_log=/var/log/nginx/shop4-dev_php_errors.log";
        fastcgi_buffers 16 16k;
        fastcgi_buffer_size 32k;
        include fastcgi_params;
    }

    #minify rules
    rewrite ^/includes/libs/minify/([a-z]=.*) /includes/libs/minify/index.php?$1 last;
    rewrite ^/asset/(.*) /includes/libs/minify/index.php?g=$1 last;

    location / {
        try_files $uri $uri/ /index.php?q=$uri&$args;
    }

    location /classes {
        location ~ \.php$ {
            deny all;
        }
    }

    location /templates {
        location ~ \.php$ {
            deny all;
        }
        location ~ \.tpl$ {
            deny all;
        }
    }

    location /shop4-devlogs {
        deny all;
    }

    location /update {
        deny all;
    }

    location /uploads {
        deny all;
    }
}
